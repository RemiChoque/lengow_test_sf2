<?php

namespace TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('order_id')
            ->add('marketplace')
            ->add('idFlux')
            ->add('order_status_marketplace')
            ->add('order_status_lengow')
            ->add('order_purchase_datetime')
            ->add('order_amount')
            ->add('order_tax')
            ->add('order_shipping')
            ->add('order_currency')
            ->add('customer_id')
            ->add('billing_address', 'entity', array(
                'class' => 'TestBundle:Address',
                'choice_label' => 'address_1'
                ))
            ->add('delivery_address', 'entity', array(
                'class' => 'TestBundle:Address',
                'choice_label' => 'address_1'
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\Order'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'testbundle_order';
    }
}
