<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="TestBundle\Entity\Repository\AddressRepository")
 * @GRID\Source(columns="id, lastname, firstname, address_1, zipcode, city")
 */
class Address
{
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
     * @var string
     *
     * @ORM\Column(name="civility", type="string", length=25, nullable=true)
     */
	private $civility;

	/**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
	private $lastname;

	/**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
	private $firstname;

	/**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     * @Assert\Email(
     *     message = "L'email '{{ value }}' n'est pas valide.",
     *     checkMX = true
     * )
     */
	private $email;

	/**
     * @var string
     *
     * @ORM\Column(name="address_1", type="string", nullable=true)
     */
	private $address_1;

	/**
     * @var string
     *
     * @ORM\Column(name="address_2", type="string", nullable=true)
     */
	private $address_2;

	/**
     * @var string
     *
     * @ORM\Column(name="address_3", type="string", nullable=true)
     */
	private $address_3;

	/**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", nullable=true)
     */
	private $zipcode;

	/**
     * @var string
     *
     * @ORM\Column(name="city", type="string", nullable=true)
     */
	private $city;

	/**
     * @var string
     *
     * @ORM\Column(name="country", type="string", nullable=true)
     */
	private $country;
	
	/**
     * @var string
     *
     * @ORM\Column(name="phone_home", type="string", nullable=true)
     */
	private $phone_home;
	
	/**
     * @var string
     *
     * @ORM\Column(name="phone_mobile", type="string", nullable=true)
     */
	private $phone_mobile;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set civility
     *
     * @param string $civility
     * @return Address
     */
    public function setCivility($civility)
    {
        $this->civility = $civility;

        return $this;
    }

    /**
     * Get civility
     *
     * @return string 
     */
    public function getCivility()
    {
        return $this->civility;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Address
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Address
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Address
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address_1
     *
     * @param string $address1
     * @return Address
     */
    public function setAddress1($address1)
    {
        $this->address_1 = $address1;

        return $this;
    }

    /**
     * Get address_1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address_1;
    }

    /**
     * Set address_2
     *
     * @param string $address2
     * @return Address
     */
    public function setAddress2($address2)
    {
        $this->address_2 = $address2;

        return $this;
    }

    /**
     * Get address_2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address_2;
    }

    /**
     * Set address_3
     *
     * @param string $address3
     * @return Address
     */
    public function setAddress3($address3)
    {
        $this->address_3 = $address3;

        return $this;
    }

    /**
     * Get address_3
     *
     * @return string 
     */
    public function getAddress3()
    {
        return $this->address_3;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     * @return Address
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string 
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Address
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phone_home
     *
     * @param string $phoneHome
     * @return Address
     */
    public function setPhoneHome($phoneHome)
    {
        $this->phone_home = $phoneHome;

        return $this;
    }

    /**
     * Get phone_home
     *
     * @return string 
     */
    public function getPhoneHome()
    {
        return $this->phone_home;
    }

    /**
     * Set phone_mobile
     *
     * @param string $phoneMobile
     * @return Address
     */
    public function setPhoneMobile($phoneMobile)
    {
        $this->phone_mobile = $phoneMobile;

        return $this;
    }

    /**
     * Get phone_mobile
     *
     * @return string 
     */
    public function getPhoneMobile()
    {
        return $this->phone_mobile;
    }
}
