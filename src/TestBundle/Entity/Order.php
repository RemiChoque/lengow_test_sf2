<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
 * Order
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="TestBundle\Entity\Repository\OrderRepository")
 * @GRID\Source(columns="order_id, marketplace, order_status_marketplace, order_amount")
 */
class Order
{
	/**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=25, nullable=false)
     * @ORM\Id
     */
	private $order_id;

	/**
     * @var string
     *
     * @ORM\Column(name="marketplace", type="string", length=255, nullable=true)
     */
	private $marketplace;
	
	/**
     * @var integer
     *
     * @ORM\Column(name="id_flux", type="integer", nullable=true)
     */
	private $idFlux;

	/**
     * @var string
     *
     * @ORM\Column(name="order_status_marketplace", type="string", length=255, nullable=true)
     */
	private $order_status_marketplace;

	/**
     * @var string
     *
     * @ORM\Column(name="order_status_lengow", type="string", length=255, nullable=true)
     */
	private $order_status_lengow;

	/**
     * @var \DateTime
     *
     * @ORM\Column(name="order_purchase_datetime", type="datetime", nullable=true)
     */
	private $order_purchase_datetime;

	/**
	 * @var decimal
	 *
	 * @ORM\Column(name="order_amount", type="decimal", nullable=true)
	 */
	private $order_amount;

	/**
	 * @var decimal
	 *
	 * @ORM\Column(name="order_tax", type="decimal", nullable=true)
	 */
	private $order_tax;

	/**
	 * @var decimal
	 *
	 * @ORM\Column(name="order_shipping", type="decimal", nullable=true)
	 */
	private $order_shipping;

	/**
     * @var string
     *
     * @ORM\Column(name="order_currency", type="string", length=255, nullable=true)
     */
	private $order_currency;
	
	/**
     * @var address
     *
     * @ORM\ManyToOne(targetEntity="Address", cascade={"remove", "persist"})
     * @ORM\JoinColumn(name="billing_address_id", referencedColumnName="id", nullable=true)
     */
	private $billing_address;

	/**
     * @var address
     *
     * @ORM\ManyToOne(targetEntity="Address", cascade={"remove", "persist"})
     * @ORM\JoinColumn(name="delivery_address_id", referencedColumnName="id", nullable=true)
     */
	private $delivery_address;

	/**
     * @var string
     *
     * @ORM\Column(name="customer_id", type="string", length=255, nullable=true)
     */
	private $customer_id;


    /**
     * Set order_id
     *
     * @param string $orderId
     * @return Order
     */
    public function setOrderId($orderId)
    {
        $this->order_id = $orderId;

        return $this;
    }

    /**
     * Get order_id
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return Order
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set idFlux
     *
     * @param integer $idFlux
     * @return Order
     */
    public function setIdFlux($idFlux)
    {
        $this->idFlux = $idFlux;

        return $this;
    }

    /**
     * Get idFlux
     *
     * @return integer 
     */
    public function getIdFlux()
    {
        return $this->idFlux;
    }

    /**
     * Set order_status_marketplace
     *
     * @param string $orderStatusMarketplace
     * @return Order
     */
    public function setOrderStatusMarketplace($orderStatusMarketplace)
    {
        $this->order_status_marketplace = $orderStatusMarketplace;

        return $this;
    }

    /**
     * Get order_status_marketplace
     *
     * @return string 
     */
    public function getOrderStatusMarketplace()
    {
        return $this->order_status_marketplace;
    }

    /**
     * Set order_status_lengow
     *
     * @param string $orderStatusLengow
     * @return Order
     */
    public function setOrderStatusLengow($orderStatusLengow)
    {
        $this->order_status_lengow = $orderStatusLengow;

        return $this;
    }

    /**
     * Get order_status_lengow
     *
     * @return string 
     */
    public function getOrderStatusLengow()
    {
        return $this->order_status_lengow;
    }

    /**
     * Set order_purchase_datetime
     *
     * @param \DateTime $orderPurchaseDatetime
     * @return Order
     */
    public function setOrderPurchaseDatetime($orderPurchaseDatetime)
    {
        $this->order_purchase_datetime = $orderPurchaseDatetime;

        return $this;
    }

    /**
     * Get order_purchase_datetime
     *
     * @return \DateTime 
     */
    public function getOrderPurchaseDatetime()
    {
        return $this->order_purchase_datetime;
    }

    /**
     * Set order_amount
     *
     * @param string $orderAmount
     * @return Order
     */
    public function setOrderAmount($orderAmount)
    {
        $this->order_amount = $orderAmount;

        return $this;
    }

    /**
     * Get order_amount
     *
     * @return string 
     */
    public function getOrderAmount()
    {
        return $this->order_amount;
    }

    /**
     * Set order_tax
     *
     * @param string $orderTax
     * @return Order
     */
    public function setOrderTax($orderTax)
    {
        $this->order_tax = $orderTax;

        return $this;
    }

    /**
     * Get order_tax
     *
     * @return string 
     */
    public function getOrderTax()
    {
        return $this->order_tax;
    }

    /**
     * Set order_shipping
     *
     * @param string $orderShipping
     * @return Order
     */
    public function setOrderShipping($orderShipping)
    {
        $this->order_shipping = $orderShipping;

        return $this;
    }

    /**
     * Get order_shipping
     *
     * @return string 
     */
    public function getOrderShipping()
    {
        return $this->order_shipping;
    }

    /**
     * Set order_currency
     *
     * @param string $orderCurrency
     * @return Order
     */
    public function setOrderCurrency($orderCurrency)
    {
        $this->order_currency = $orderCurrency;

        return $this;
    }

    /**
     * Get order_currency
     *
     * @return string 
     */
    public function getOrderCurrency()
    {
        return $this->order_currency;
    }

    /**
     * Set customer_id
     *
     * @param string $customerId
     * @return Order
     */
    public function setCustomerId($customerId)
    {
        $this->customer_id = $customerId;

        return $this;
    }

    /**
     * Get customer_id
     *
     * @return string 
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Set billing_address
     *
     * @param \TestBundle\Entity\Address $billingAddress
     * @return Order
     */
    public function setBillingAddress(\TestBundle\Entity\Address $billingAddress = null)
    {
        $this->billing_address = $billingAddress;

        return $this;
    }

    /**
     * Get billing_address
     *
     * @return \TestBundle\Entity\Address 
     */
    public function getBillingAddress()
    {
        return $this->billing_address;
    }

    /**
     * Set delivery_address
     *
     * @param \TestBundle\Entity\Address $deliveryAddress
     * @return Order
     */
    public function setDeliveryAddress(\TestBundle\Entity\Address $deliveryAddress = null)
    {
        $this->delivery_address = $deliveryAddress;

        return $this;
    }

    /**
     * Get delivery_address
     *
     * @return \TestBundle\Entity\Address 
     */
    public function getDeliveryAddress()
    {
        return $this->delivery_address;
    }
}
