<?php
namespace TestBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use TestBundle\Entity\Order;
use TestBundle\Entity\Address;

class ImportOrdersCommand extends Command
{

	private $em;

	private $url_orders;

	private $logger;

	public function __construct($doctrine, $url_orders, $logger)
	{
		$this->em = $doctrine->getManager();
		$this->url_orders = $url_orders;
		$this->logger = $logger;

		parent::__construct();
	}

	protected function configure()
	{
		$this->setName('lengow:test:import');
		$this->setDescription('Importe les commandes');
	}

	public function execute(InputInterface $input, OutputInterface $output)
	{
		$this->logger->info('Récupération du fichier XML');
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->url_orders);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$xml = curl_exec($curl);

		$this->logger->info('Lecture du fichier XML');
		$crawler = new Crawler($xml);
		$orders_xml = $crawler->filter('orders')->children();

		$orderRepository = $this->em->getRepository('TestBundle:Order');
		$nb_new_orders = 0;
		foreach($orders_xml as $order_xml){
			$order_id = $order_xml->getElementsByTagName('order_id')->item(0)->nodeValue;
			$order = $orderRepository->findOneBy(array('order_id' => $order_id));
			if($order==null){
				$this->logger->info('Création de la commande '.$order_id);
				$order = new Order();
				$order->setOrderId($order_id);
				$nb_new_orders++;
			}else{
				$this->logger->info('La commande '.$order_id.' existe déjà.');
			}
			$marketplace = $order_xml->getElementsByTagName('marketplace')->item(0)->nodeValue;
			$order->setMarketplace($marketplace);
			$idFlux = $order_xml->getElementsByTagName('idFlux')->item(0)->nodeValue;
			$order->setIdFlux($idFlux);
			$orderStatusMarketplace = $order_xml->getElementsByTagName('order_status')->item(0)->getElementsByTagName('marketplace')->item(0)->nodeValue;
			$order->setOrderStatusMarketplace($orderStatusMarketplace);
			$orderStatusLengow = $order_xml->getElementsByTagName('order_status')->item(0)->getElementsByTagName('lengow')->item(0)->nodeValue;
			$order->setOrderStatusLengow($orderStatusLengow);
			$order_purchase_date = $order_xml->getElementsByTagName('order_purchase_date')->item(0)->nodeValue;
			$order_purchase_heure = $order_xml->getElementsByTagName('order_purchase_heure')->item(0)->nodeValue;
			if($order_purchase_date!=null && $order_purchase_heure!=null){
				$orderPurchaseDatetime = \DateTime::createFromFormat('Y-m-d_H:i:s', $order_purchase_date."_".$order_purchase_heure);
				$order->setOrderPurchaseDatetime($orderPurchaseDatetime);
			}
			$order_amount = floatval($order_xml->getElementsByTagName('order_amount')->item(0)->nodeValue);
			$order->setOrderAmount($order_amount);
			$order_tax = floatval($order_xml->getElementsByTagName('order_tax')->item(0)->nodeValue);
			$order->setOrderTax($order_tax);
			$order_shipping = floatval($order_xml->getElementsByTagName('order_shipping')->item(0)->nodeValue);
			$order->setOrderShipping($order_shipping);
			$order_currency = $order_xml->getElementsByTagName('order_currency')->item(0)->nodeValue;
			$order->setOrderCurrency($order_currency);
			$customer_id = $order_xml->getElementsByTagName('customer_id')->item(0)->nodeValue;
			$order->setCustomerId($customer_id);

			$billing_address = $order_xml->getElementsByTagName('billing_address')->item(0);

			$billingAddress = new Address();
			$civility = $billing_address->getElementsByTagName('billing_civility')->item(0)->nodeValue;
			$billingAddress->setCivility($civility);
			$lastname = $billing_address->getElementsByTagName('billing_lastname')->item(0)->nodeValue;
			$billingAddress->setLastname($lastname);
			$firstname = $billing_address->getElementsByTagName('billing_firstname')->item(0)->nodeValue;
			$billingAddress->setFirstname($firstname);
			$email = $billing_address->getElementsByTagName('billing_email')->item(0)->nodeValue;
			$billingAddress->setEmail($email);
			$address1 = $billing_address->getElementsByTagName('billing_address')->item(0)->nodeValue;
			$billingAddress->setAddress1($address1);
			$address2 = $billing_address->getElementsByTagName('billing_address_2')->item(0)->nodeValue;
			$billingAddress->setAddress2($address2);
			$address3 = $billing_address->getElementsByTagName('billing_address_complement')->item(0)->nodeValue;
			$billingAddress->setAddress3($address3);
			$zipcode = $billing_address->getElementsByTagName('billing_zipcode')->item(0)->nodeValue;
			$billingAddress->setZipcode($zipcode);
			$city = $billing_address->getElementsByTagName('billing_city')->item(0)->nodeValue;
			$billingAddress->setCity($city);
			$country = $billing_address->getElementsByTagName('billing_country')->item(0)->nodeValue;
			$billingAddress->setCountry($country);
			$phoneHome = $billing_address->getElementsByTagName('billing_phone_home')->item(0)->nodeValue;
			$billingAddress->setPhoneHome($phoneHome);
			$phoneMobile = $billing_address->getElementsByTagName('billing_phone_mobile')->item(0)->nodeValue;
			$billingAddress->setPhoneMobile($phoneMobile);   
			$order->setBillingAddress($billingAddress);

			$delivery_address = $order_xml->getElementsByTagName('delivery_address')->item(0);

			$deliveryAddress = new Address();
			$civility = $delivery_address->getElementsByTagName('delivery_civility')->item(0)->nodeValue;
			$deliveryAddress->setCivility($civility);
			$lastname = $delivery_address->getElementsByTagName('delivery_lastname')->item(0)->nodeValue;
			$deliveryAddress->setLastname($lastname);
			$firstname = $delivery_address->getElementsByTagName('delivery_firstname')->item(0)->nodeValue;
			$deliveryAddress->setFirstname($firstname);
			$email = $delivery_address->getElementsByTagName('delivery_email')->item(0)->nodeValue;
			$deliveryAddress->setEmail($email);
			$address1 = $delivery_address->getElementsByTagName('delivery_address')->item(0)->nodeValue;
			$deliveryAddress->setAddress1($address1);
			$address2 = $delivery_address->getElementsByTagName('delivery_address_2')->item(0)->nodeValue;
			$deliveryAddress->setAddress2($address2);
			$address3 = $delivery_address->getElementsByTagName('delivery_address_complement')->item(0)->nodeValue;
			$deliveryAddress->setAddress3($address3);
			$zipcode = $delivery_address->getElementsByTagName('delivery_zipcode')->item(0)->nodeValue;
			$deliveryAddress->setZipcode($zipcode);
			$city = $delivery_address->getElementsByTagName('delivery_city')->item(0)->nodeValue;
			$deliveryAddress->setCity($city);
			$country = $delivery_address->getElementsByTagName('delivery_country')->item(0)->nodeValue;
			$deliveryAddress->setCountry($country);
			$phoneHome = $delivery_address->getElementsByTagName('delivery_phone_home')->item(0)->nodeValue;
			$deliveryAddress->setPhoneHome($phoneHome);
			$phoneMobile = $delivery_address->getElementsByTagName('delivery_phone_mobile')->item(0)->nodeValue;
			$deliveryAddress->setPhoneMobile($phoneMobile);
			$order->setDeliveryAddress($deliveryAddress);
			$this->em->persist($order);
		}
		$this->em->flush();
		$this->logger->info($nb_new_orders.' nouvelles commandes créées');
	}
}