<?php

namespace TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;
use TestBundle\Entity\Order;
use TestBundle\Form\OrderType;

/**
 * Order controller.
 *
 */
class OrderController extends Controller
{

    public function apiListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $orders = $em->getRepository('TestBundle:Order')->findAll();

        $format = $request->get('_format');
        $data = $this->container->get('serializer')->serialize($orders, $format);
        $response = new Response();
        $response->headers->set('Content-type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    public function apiShowAction(Request $request, $order_id)
    {
        $em = $this->getDoctrine()->getManager();

        $order = $em->getRepository('TestBundle:Order')->find($order_id);

        if (!$order) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

        $format = $request->get('_format');
        $data = $this->container->get('serializer')->serialize($order, $format);
        $response = new Response();
        $response->headers->set('Content-type', 'application/json');
        $response->setContent($data);
        return $response;
    }

    /**
     * Lists all Order entities.
     *
     */
    public function indexAction()
    {
        $source = new Entity('TestBundle:Order');
        $grid = $this->get('grid');
        $grid->setSource($source);
        $showRowAction = new RowAction('Show', 'order_show');
        $showRowAction->setRouteParameters(array('order_id'));
        $grid->addRowAction($showRowAction);
        $showRowAction = new RowAction('Json', 'api_orders_show');
        $showRowAction->setRouteParameters(array('order_id', '_format'=>'json'));
        $grid->addRowAction($showRowAction);
        $showRowAction = new RowAction('Yml', 'api_orders_show');
        $showRowAction->setRouteParameters(array('order_id', '_format'=>'yml'));
        $grid->addRowAction($showRowAction);
        $editRowAction = new RowAction('Edit', 'order_edit');
        $editRowAction->setRouteParameters(array('order_id'));
        $grid->addRowAction($editRowAction);
        $deleteRowAction = new RowAction('Delete', 'order_delete');
        $deleteRowAction->setRouteParameters(array('order_id'));
        $grid->addRowAction($deleteRowAction);
        $grid->isReadyForRedirect();

        return $this->render('TestBundle:Order:index.html.twig', array(
            'grid' => $grid,
        ));
    }
    /**
     * Creates a new Order entity.
     *
     */
    public function newAction(Request $request)
    {
        $entity = new Order();
        $form = $this->createForm(new OrderType(), $entity, array());
        $form->add('submit', 'submit', array('label' => 'Create'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('order_show', array('order_id' => $entity->getOrderId())));
        }

        return $this->render('TestBundle:Order:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Order entity.
     *
     */
    public function showAction($order_id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Order')->find($order_id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

        $deleteForm = $this->createDeleteForm($order_id);

        return $this->render('TestBundle:Order:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Order entity.
     *
     */
    public function editAction($order_id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TestBundle:Order')->find($order_id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Order entity.');
        }

        $editForm = $this->createForm(new OrderType(), $entity, array());
        $editForm->add('submit', 'submit', array('label' => 'Update'));
        $deleteForm = $this->createDeleteForm($order_id);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('order_edit', array('order_id' => $order_id)));
        }

        return $this->render('TestBundle:Order:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Order entity.
     *
     */
    public function deleteAction(Request $request, $order_id)
    {
        $form = $this->createDeleteForm($order_id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TestBundle:Order')->find($order_id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Order entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('order_index'));
    }

    /**
     * Creates a form to delete a Order entity by id.
     *
     * @param mixed $order_id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($order_id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('order_delete', array('order_id' => $order_id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
