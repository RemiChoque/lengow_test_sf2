<?php
namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        return $this->render('TestBundle::index.html.twig', array());
    }
}